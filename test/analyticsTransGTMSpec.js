var assert = require('assert');
var sinon = require('sinon');

var MockBrowser = require('mock-browser').mocks.MockBrowser;
var mock = new MockBrowser();
global.window = mock.getWindow();
global.window.document = mock.getDocument();
global.window.dataLayer = [];

var atGTM = require('../analyticsTransGTM');
var transInstance = atGTM();

describe('analyticsTransGTM', function () {
  it('should have analyticstracker', function () {
    assert.equal(typeof transInstance.tracker, 'object');
  });

  describe('sendPageImpressionEvent', function() {
    it('should pass page impression event', function(done) {
      var testEvent = {"event" : "page-impression", "info" : {"name" : "test"}, "commerce" : {"name" :"testcommerce"}};
      var resultEvent = {"event" : "page-impression", "pageInfo" : {"name" : "test"}, "commerce" : {"name" :"testcommerce"}}

      transInstance.tracker.trackEvent(testEvent);

      function testDataLayer() {
        assert.deepStrictEqual(global.window.dataLayer[0], resultEvent);
        done();
      }
      setTimeout(testDataLayer, 100);
    });
  });

  describe('sendOtherEventBeforePageImpressionEvent', function() {
    it('should pass first the page impression and then the other event', function(done) {
      var pageEvent = {"event" : "page-impression", "info" : {"name" : "test"}, "commerce" : {"name" :"testcommerce"}};
      var testEvent = {"event" : "some-other-event", "info" : {"name" : "other"}, "commerce" : {"name" :"testcommerce"}};
      var pageResultEvent = {"event" : "page-impression", "pageInfo" : {"name" : "test"}, "commerce" : {"name" :"testcommerce"}}
      var otherResultEvent = {"event" : "some-other-event", "pageInfo" : {"name" : "test"}, "info" : {"name" : "other"}, "commerce" : {"name" :"testcommerce"}}

      // reset the dataLayer
      transInstance.tracker.resetTracker();
      global.window.dataLayer = [];
      transInstance.tracker.trackEvent(testEvent);
      transInstance.tracker.trackEvent(pageEvent);

      function testDataLayer() {
        assert.deepStrictEqual(global.window.dataLayer[0], pageResultEvent);
        assert.deepStrictEqual(global.window.dataLayer[1], otherResultEvent);
        done();
      }
      setTimeout(testDataLayer, 100);
    });
  });
});
